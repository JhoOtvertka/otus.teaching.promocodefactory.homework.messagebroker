﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.BrokerContracts.GivingPromoCodeToCustomer;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.MassTransit.ApplicationEvents
{
    public class ApplicationGivePromoCodeToCustomerConsumer : IConsumer<GivingPromoCodeToCustomer>
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Preference> _preferencesRepository;
        private readonly IRepository<Customer> _customersRepository;
        private readonly ILogger<ApplicationGivePromoCodeToCustomerConsumer> _logger;
        private readonly IMapper _mapper;

        public ApplicationGivePromoCodeToCustomerConsumer(
            IMapper mapper, 
            ILogger<ApplicationGivePromoCodeToCustomerConsumer> logger, 
            IRepository<PromoCode> promoCodesRepository, 
            IRepository<Preference> preferencesRepository, 
            IRepository<Customer> customersRepository)
        {
            _mapper = mapper;
            _logger = logger;
            _promoCodesRepository = promoCodesRepository;
            _preferencesRepository = preferencesRepository;
            _customersRepository = customersRepository;
        }

        public async Task Consume(ConsumeContext<GivingPromoCodeToCustomer> context)
        {
            var model = _mapper.Map<GivingPromoCodeToCustomer>(context.Message);
            var preference = await _preferencesRepository.GetByIdAsync(model.PreferenceId);
            if (preference != null)
            {
                var customers = await _customersRepository
                    .GetWhere(d => d.Preferences.Any(x =>
                        x.Preference.Id == preference.Id));

                PromoCode promoCode = PromoCodeMapper.MapFromModel(_mapper.Map<GivingPromoCodeToCustomer, GivePromoCodeRequest>(model), preference, customers);

                await _promoCodesRepository.AddAsync(promoCode);
            }
            else
            {
                _logger.LogInformation("Get message from broker. Create user: {@User}", model);
            }
        }

    }
}