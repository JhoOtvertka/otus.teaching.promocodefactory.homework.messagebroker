﻿using BrokerContracts;
using MassTransit.Definition;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.MassTransit.ApplicationEvents;

namespace Otus.Teaching.Pcf.Administration.WebHost.MassTransit.ApplicationEvents
{
    public class ApplicationPromocodeConsumerDefinition
    {
        public class ApplicationGivePromoCodeToCustomerConsumerDefinition : ConsumerDefinition<ApplicationGivePromoCodeToCustomerConsumer>
        {
            public ApplicationGivePromoCodeToCustomerConsumerDefinition()
            {
                EndpointName = Constants.NotificationQueueCore;
            }
        }
    }
}