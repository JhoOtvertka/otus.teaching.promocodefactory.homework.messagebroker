﻿using BrokerContracts;
using MassTransit.Definition;

namespace Otus.Teaching.Pcf.Administration.WebHost.MassTransit.ApplicationEvents
{
    public class ApplicationPromocodeConsumerDefinition
    {
        public class ApplicationUpdateAppliedPromocodesConsumerDefinition : ConsumerDefinition<ApplicationUpdateAppliedPromocodesConsumer>
        {
            public ApplicationUpdateAppliedPromocodesConsumerDefinition()
            {
                EndpointName = Constants.NotificationQueueCore;
            }
        }

    }
}