﻿using System.Threading.Tasks;
using AutoMapper;
using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using Otus.Teaching.Pcf.BrokerContracts.Administration;

namespace Otus.Teaching.Pcf.Administration.WebHost.MassTransit.ApplicationEvents
{
    public class ApplicationUpdateAppliedPromocodesConsumer : IConsumer<UpdateAppliedPromocodes>
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly ILogger<ApplicationUpdateAppliedPromocodesConsumer> _logger;
        private readonly IMapper _mapper;

        public ApplicationUpdateAppliedPromocodesConsumer(IRepository<Employee> employeeRepository, IMapper mapper,
            ILogger<ApplicationUpdateAppliedPromocodesConsumer> logger)
        {
            _employeeRepository = employeeRepository;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<UpdateAppliedPromocodes> context)
        {
            var model = _mapper.Map<UpdateAppliedPromocodes>(context.Message);
            _logger.LogInformation("Get message from broker. : UpdateAppliedPromocode {@Id}", model);
            var employeeToUpdate = await _employeeRepository.GetByIdAsync(model.Id);
            if(employeeToUpdate != null)
            {
                employeeToUpdate.AppliedPromocodesCount++;

                await _employeeRepository.UpdateAsync(employeeToUpdate);
            }
            else
            {
                _logger.LogError("employee not found", model);
            }
            
        }

    }
}