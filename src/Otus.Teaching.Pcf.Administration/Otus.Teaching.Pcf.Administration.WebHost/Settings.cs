﻿using System;

namespace Otus.Teaching.Pcf.Administration.WebHost
{
    public static class Settings
    {
        public static class MassTransit
        {
            public static readonly string Url = Environment.GetEnvironmentVariable("MASSTRANSIT_URL");
            public static readonly string Host = Environment.GetEnvironmentVariable("MASSTRANSIT_HOST");
            public static readonly string UserName = Environment.GetEnvironmentVariable("MASSTRANSIT_USERNAME");
            public static readonly string Password = Environment.GetEnvironmentVariable("MASSTRANSIT_PASSWORD");
        }
    }
}
